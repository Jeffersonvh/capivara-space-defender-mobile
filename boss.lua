function boss_load()
   boss_img = love.graphics.newImage ('pictures/boss.png')
   --musicaBoss = love.audio.newSource("sounds/Boss.mp3",)
   max_bos_tiro = 0.7
   tempo_tiro = max_bos_tiro
   tempo_max_boss_time = 30
   tempo_boss_time = tempo_max_boss_time
   boss_vivo = false
   ultimo_level = 1
   back = false
   random1 = false
 end

function reset_game()
  --zerando o reto do jogo
  tempoInimigo = tempoMaxInimigo
  tempoAsteroide = tempoMaxAsteroide
  tempoInimigoV = tempoMaxInimigoV
  tempoInimigoH = tempoMaxInimigoH
end

function boss_update(dt,x)
  random1 = math.random(0,150)
  if (vivo == true and not pause) then
  	tempo_boss_time = tempo_boss_time - dt
  end

  if (tempo_boss_time < 0 and boss_vivo == false) then
  	boss = {x=0, y=30, life=30*level, velocidade = 300+100*level, img=boss_img}
 	  boss_vivo = true
  end

  if level == 5 then
    max_bos_tiro = 0.5
  end 

  if level == 10 then
    max_bos_tiro = 0.3
  end 

  if boss_vivo == true then
    
    reset_game()
    
    if boss.life > 0 then
      boss_tiro(dt)
      
      if back == false then
      	boss.x = boss.x + (boss.velocidade * dt)
      	if boss.x+boss.img:getWidth()/2 > love.graphics.getWidth( ) then
      		back = true
      	end
      end
      
      if random1 == 100 then
          if back == true then
          	back = false
          else
          	back = true
          end
      end
      
      if back == true then
          boss.x = boss.x - (boss.velocidade * dt)
      	if boss.x < 0-boss.img:getWidth()/2 then
      		back = false
      	end
      end
    end

    if boss.life <= 0 then
      tempo_boss_time = tempo_max_boss_time
      level = level+1;
      boss_vivo = false
      imagem_atual.y = 850
      tirosInimigo = {}
    end

  end

end

function boss_tiro(dt)
    tempo_tiro = tempo_tiro - (1 * dt)
    if tempo_tiro < 0 then
      boss_shot = true
    end

    if boss_shot == true then
      novoTiro = { x = boss.x + (boss.img:getWidth()/2), y = boss.y, img = tiroInimigoImg }
      table.insert(tirosInimigo, novoTiro)
      boss_shot = false
      
      if level == 1 then 
		tempo_tiro = math.random(1, 7)/10
	  end
	  if level == 2 then
	  	tempo_tiro = math.random(1, 5)/10
	  end
	  if level >= 3 then
	  	tempo_tiro = math.random(1, 3)/10
	  end
    end
end  

function boss_draw()
	
  if boss_vivo == true then
    if (boss.life > 0) then
      love.graphics.draw(boss.img, boss.x, boss.y)
    end
    if  boss.life <= 0 then
      love.graphics.print("Parabéns, você passou para a fase " .. tostring(level+1), love.graphics:getWidth()/2-100, love.graphics:getHeight()/2-30)
      tempo_parado = true
    end

    if boss.life <= 0 and level >= 3 and modesurvive == false then
      win = true
      vivo = false
    end
  end
end