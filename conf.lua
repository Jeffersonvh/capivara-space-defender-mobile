function love.conf(t)
	t.title = "Capivara Space Defender"
	t.window.fullscreen = true
	t.console = false
	t.identity = "Save and Load"
	t.window.icon = "pictures/icon.png"
	t.accelerometerjoystick = true
end